<html>
<head>
    <title>CodeIgniter Tutorial</title>
    <link rel="stylesheet" href="<?php echo('/CodeIgniter/assets/css/semantic.min.css') ?> " type="text/css" />
    <link rel="stylesheet" href="<?php echo('/CodeIgniter/assets/css/styles/jqx.base.css') ?> " type="text/css" />
    <script type="text/javascript" src="<?php echo('/CodeIgniter/assets/js/jquery-1.11.1.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo('/CodeIgniter/assets/js/jqx-all.js') ?>"></script>
    <script type="text/javascript" src="<?php echo('/CodeIgniter/assets/js/semantic.min.js') ?>"></script>
</head>
<body>

